//
//  DetailWeatherVC.swift
//  weatherPrivalia
//
//  Created by Cristian Espinosa Ruiz on 12/05/2019.
//  Copyright © 2019 Cristian Espinosa. All rights reserved.
//

import UIKit
import Kingfisher

class DetailWeatherVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var weather : Weather!
    
    override func loadView() {
        let cView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        cView.backgroundColor = .white
        cView.backgroundColor =  UIColor(patternImage: UIImage(named: "bg")!)
        
        let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: cView.frame.size.width, height: cView.frame.size.height))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        cView.addSubview(tableView)
        
        self.view = cView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "EEEE"
        
        self.title = dateFormatter.string(from: weather.date!.first ?? Date())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    init(weather : Weather) {
        self.weather = weather
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //    MARK: Tableview delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weather.description?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "Cell")
        cell.textLabel?.text = weather.main![indexPath.row]
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.textLabel?.textColor = .white
        let url = "https://openweathermap.org/img/w/\(weather.icon![indexPath.row]).png"
        cell.imageView?.kf.setImage(with: URL(string: url))
        let hour = Calendar.current.component(.hour, from: weather.date![indexPath.row])
        cell.detailTextLabel?.textColor = .white
        cell.detailTextLabel?.text = "\(hour):00"
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

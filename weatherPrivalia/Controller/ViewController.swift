//
//  ViewController.swift
//  weatherPrivalia
//
//  Created by Cristian Espinosa Ruiz on 11/05/2019.
//  Copyright © 2019 Cristian Espinosa. All rights reserved.
//

import UIKit
import Kingfisher

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var weatherCollection : WeatherCollection!
    var mainImage : UIImageView!
    var lblDetail : UILabel!
    var tableView: UITableView!
    
    override func loadView() {
        let cView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        cView.backgroundColor  = .white
        cView.backgroundColor = UIColor(patternImage: UIImage(named: "bg")!)
        
        let lblTitle = UILabel(frame: CGRect(x: 0, y: cView.frame.size.height*0.1, width: cView.frame.size.width, height: 50))
        lblTitle.text = "París"
        lblTitle.textAlignment = .center
        lblTitle.font = UIFont.systemFont(ofSize: 40)
        lblTitle.textColor = .white
        cView.addSubview(lblTitle)

        lblDetail = UILabel(frame: CGRect(x: 0, y: cView.frame.size.height*0.1 + 55, width: cView.frame.size.width, height: 50))
        lblDetail.textAlignment = .center
        lblDetail.font = UIFont.systemFont(ofSize: 40)
        lblDetail.textColor = .white
        cView.addSubview(lblDetail)
        
        mainImage = UIImageView(frame: CGRect(x: 0, y: 0, width: cView.frame.size.width*0.3, height: cView.frame.size.height*0.3))
        mainImage.contentMode = .scaleAspectFit
        cView.addSubview(mainImage)
        
        tableView = UITableView(frame: CGRect(x: 0, y: cView.frame.size.height*0.3, width: cView.frame.size.width, height: cView.frame.size.height*0.7))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        cView.addSubview(tableView)
        
        self.view = cView
    }
    
    override func viewDidAppear(_ animated: Bool) {
        APIService.shared
            .getWeather { (result) in
                if result != nil {
                    self.weatherCollection = result
                    self.lblDetail.text = self.weatherCollection.weatherItems.first!.main?.first!
                   let url = "https://openweathermap.org/img/w/\(self.weatherCollection.weatherItems.first!.icon!.first!).png"
                    self.mainImage.kf.setImage(with: URL(string: url))
                    self.tableView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("Weather.json")
        do {
            let data = try Data(contentsOf: fileUrl, options: [])
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            self.weatherCollection = WeatherCollection(fromJson: json)
            if self.weatherCollection != nil{
                self.tableView.reloadData()
                self.lblDetail.text = self.weatherCollection.weatherItems.first!.main?.first!
            }
        } catch {
            print(error)
        }
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if weatherCollection != nil{
            return self.weatherCollection.weatherItems.count
        }else{
            return 0
        }
    }
    
    let dateFormatter = DateFormatter()
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "Cell")
        dateFormatter.dateFormat  = "EEEE"

        cell.textLabel?.text = dateFormatter.string(from: weatherCollection.weatherItems[indexPath.row].date?.first ?? Date())
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.textLabel?.textColor = .white
        cell.detailTextLabel?.text = weatherCollection.weatherItems[indexPath.row].main?.first
        cell.detailTextLabel?.textColor = .white
        
        return cell
    }
    
    func getDayOfWeek(today:Date) -> Int? {

        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: today)
        return weekDay
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
 self.navigationController?.pushViewController(DetailWeatherVC(weather: self.weatherCollection.weatherItems[indexPath.row]), animated: true)
    }
}


//
//  Weather.swift
//  weatherPrivalia
//
//  Created by Cristian Espinosa Ruiz on 12/05/2019.
//  Copyright © 2019 Cristian Espinosa. All rights reserved.
//

import Foundation

class WeatherCollection: NSObject {
    
    var weatherItems: [Weather]!
    
    init(fromJson json: [String:Any]){
        
        self.weatherItems = [Weather]()
        let item = json["list"] as! [[String:Any]]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        var date = dateFormatter.date(from:item.first?["dt_txt"] as! String)
        let calendar = Calendar.current
        var currentDay = calendar.component(.day, from: date ?? Date())
        var description = [String]()
        var icon = [String]()
        var id = [Int]()
        var main = [String]()
        var dateArray = [Date]()

        for items in item{
            let weatherArray = items["weather"] as! [[String:Any]]
            let weather = weatherArray.first

            date = dateFormatter.date(from:items["dt_txt"] as! String)

            let day = calendar.component(.day, from: date ?? Date())
            if currentDay == day {
                description.append( weather?["description"] as! String)
                icon.append( weather?["icon"] as! String)
                id.append( weather?["id"] as! Int)
                main.append( weather?["main"] as! String)
                dateArray.append(date ?? Date())
            }else{
                let item = Weather(description:  description, icon: icon, id: id, main: main, date: dateArray)
                weatherItems.append(item)
                description.removeAll()
                icon.removeAll()
                id.removeAll()
                main.removeAll()
                dateArray.removeAll()

                currentDay = calendar.component(.day, from: date ?? Date())

                description.append( weather?["description"] as! String)
                icon.append( weather?["icon"] as! String)
                id.append( weather?["id"] as! Int)
                main.append( weather?["main"] as! String)
                dateArray.append(date ?? Date())
            }

        }
        
    }
}

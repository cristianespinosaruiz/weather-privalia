//
//  APIService.swift
//  weatherPrivalia
//
//  Created by Cristian Espinosa Ruiz on 11/05/2019.
//  Copyright © 2019 Cristian Espinosa. All rights reserved.
//

import Foundation
import Alamofire

class APIService {
    
    static let shared = APIService()
    let headers = [
        "cache-control": "no-cache",
        "Postman-Token": "87931149-4893-4ae4-bdae-ae9a11d30026"
    ]
    
    let request = NSMutableURLRequest(url: NSURL(string: "https://api.openweathermap.org/data/2.5/forecast?id=4125402&appid=728164c9d152c5f7a28c873d46914aaa")! as URL,
                                      cachePolicy: .useProtocolCachePolicy,
                                      timeoutInterval: 10.0)
    
    func getWeather(completionHandler: @escaping(WeatherCollection?) -> Void){
        Alamofire.request(request.url?.absoluteString ?? "", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            if let response = response.response, let authKey = response.allHeaderFields["Authorization"] as? String {
            }

            switch response.result {
            case .success:
                if let json = response.result.value as? [String:Any] {
                    guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
                    let fileUrl = documentDirectoryUrl.appendingPathComponent("Weather.json")
                    do {
                        let data = try JSONSerialization.data(withJSONObject: json, options: [])
                        try data.write(to: fileUrl, options: [])
                    } catch {
                        print(error)
                    }
                    completionHandler(WeatherCollection(fromJson: json))
                }
            case .failure:
                completionHandler(nil)
            }
        }
    }
}

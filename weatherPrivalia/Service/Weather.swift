//
//  Weather.swift
//  weatherPrivalia
//
//  Created by Cristian Espinosa Ruiz on 12/05/2019.
//  Copyright © 2019 Cristian Espinosa. All rights reserved.
//

import Foundation

final class Weather {
    
    var id : [Int]?
    var description : [String]?
    var icon : [String]?
    var main : [String]?
    var date : [Date]?
    
    init(description: [String], icon : [String],id:[Int], main: [String], date:[Date]) {
        self.description = description
        self.icon = icon
        self.id = id
        self.main = main
        self.date = date
    }
}
